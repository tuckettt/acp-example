public interface EightBallHandler {
    /**
     * The number that the user chooses should be pased into this method.
     */
    void result(String userChoice);
}
