import java.util.HashMap;

public class EightBallAppController {


    /**
     * Create HashMap
     */
    public static HashMap<String, EightBallHandler> eightBallMap = new HashMap<String, EightBallHandler>();

    /**
     * Here the user will choose a number between 1 and 5.
     * The number that they choose will return to the user
     * an answer to their question
     */
    public void eightBallHandlerRequest(String userChoice) {
        eightBallMap.put("one", new EightBallOne());
        //eightBallMap.put("two", new eightBallTwo());
        //eightBallMap.put("three", new eightBallThree());
        //eightBallMap.put("four", new eightBallFour());
       // eightBallMap.put("five", new eightBallFive());

        EightBallHandler eightHandle = eightBallMap.get(userChoice);

        eightHandle.result(userChoice);
    }


}
